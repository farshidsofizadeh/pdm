package com.rubytech.pdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rubytech.pdm.controller.MyAlert;
import com.rubytech.pdm.database.DatabaseAccess;
import com.rubytech.pdm.model.Patient;
import com.rubytech.pdm.model.PatientListAdapter;

import java.util.ArrayList;

public class PatientListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public static int START_PATIENT_ACTIVITY_REQUEST_CODE = 1;
    public static String PATIENT_LIST_ITEM_CLICK_PASS_NAME = "clickedPatient";
    public static String PATIENT_LIST_ITEM_POS_PASS_NAME = "patientPos";
    public static String PATIENT_ACTIVITY_TITLE_PASS_NAME = "titleName";
    private ListView listView;
    private ArrayList<Patient> patientArrayList;
    private PatientListAdapter customPatientAdapter;
    private MenuItem item;
    private SearchView actionSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_list);

        listView = (ListView) findViewById(R.id.patient_listView);

        // Attaching the layout to the toolbar object
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Patients");


        //getting patient list form db
        DatabaseAccess databaseAccess = new DatabaseAccess(this);
        patientArrayList = databaseAccess.getAllPatient();

        customPatientAdapter = new PatientListAdapter(this, R.layout.custom_patient_item, patientArrayList);
        listView.setAdapter(customPatientAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {

                Patient patient = (Patient) customPatientAdapter.getItem(position);
                Intent intent = new Intent(PatientListActivity.this, PatientActivity.class);
                intent.putExtra(PATIENT_LIST_ITEM_CLICK_PASS_NAME, patient);
                intent.putExtra(PATIENT_ACTIVITY_TITLE_PASS_NAME, "Patient Info");
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_patient_list, menu);


        item = menu.findItem(R.id.action_search_patient);

        actionSearch = (SearchView) item.getActionView();
        actionSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                ArrayList<Patient> result = new ArrayList<>();

                for (Patient p : patientArrayList) {

                    if (p.getFullName().toLowerCase().contains(newText.toLowerCase())) {
                        result.add(p);
                    }
                }

                ((PatientListAdapter) listView.getAdapter()).update(result);


                return false;
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_adding_patient:
                Intent intent = new Intent(this, PatientActivity.class);
                intent.putExtra(PATIENT_ACTIVITY_TITLE_PASS_NAME, "Add Patient");
                startActivityForResult(intent, START_PATIENT_ACTIVITY_REQUEST_CODE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == START_PATIENT_ACTIVITY_REQUEST_CODE) {
            if (resultCode == PatientListActivity.RESULT_OK) {

                Patient patient = (Patient) data.getParcelableExtra(PatientActivity.PATIENT_RESULT_PASS_NAME);
                patientArrayList.add(patient);
                customPatientAdapter.notifyDataSetChanged();
                MyAlert.myAlert("information", "Patient with registration code ( " + patient.getCode() + " ) has been added successfully", this);

            }
            if (resultCode == PatientActivity.PATIENT_CANCEL_RESULT_CODE) {
                MyAlert.myAlert("information", "Addition of new patient has been canceled successfully", this);

            }
        } else if (requestCode == PatientListAdapter.PATIENT_LIST_ACTIVITY_EDIT_REQUEST_CODE) {

            if (resultCode == PatientActivity.PATIENT_SUCCESS_UPDATE_RESULT_CODE) {

                Patient patient = (Patient) data.getParcelableExtra(PatientActivity.PATIENT_RESULT_PASS_NAME);

                DatabaseAccess databaseAccess = new DatabaseAccess(this);
                patientArrayList = databaseAccess.getAllPatient();
                customPatientAdapter.notifyDataSetChanged();
                actionSearch.setQuery("", false);
                actionSearch.clearFocus();
                MyAlert.myAlert("information", "Patient with registration code ( " + patient.getCode() + " ) has been updated successfully", this);

            } else if (resultCode == PatientActivity.PATIENT_FAILD_UPDATE_RESULT_CODE) {
                Patient patient = (Patient) data.getParcelableExtra(PatientActivity.PATIENT_RESULT_PASS_NAME);
                MyAlert.myAlert("information", "some problems occurred during updating Patient with registration code ( " + patient.getCode() + " ), please try again!", this);

            }
        }


    }


}
