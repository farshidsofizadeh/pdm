package com.rubytech.pdm;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.rubytech.pdm.database.DatabaseAccess;
import com.rubytech.pdm.model.Patient;


public class PatientActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private EditText patientNameEditText, patientCodeEditText, patientPhoneEditText, patientAddressEditText, patientDescEditText;
    private Button saveButton, cancelButton;
    public static String PATIENT_RESULT_PASS_NAME = "patientResult";
    public static String PATIENT_ACTIVITY_EDIT_PASS_NAME = "edit";
    public static int PATIENT_CANCEL_RESULT_CODE = -3;
    public static int PATIENT_SUCCESS_UPDATE_RESULT_CODE = 5;
    public static int PATIENT_FAILD_UPDATE_RESULT_CODE = 4;
    private boolean isEditMode = false;
    private Patient passPatient = null;
    private int pos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);


        // Attaching the layout to the toolbar object
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call

        setSupportActionBar(toolbar);
        Intent intent = getIntent();

        String title = intent.getStringExtra(PatientListActivity.PATIENT_ACTIVITY_TITLE_PASS_NAME);
        isEditMode = intent.getBooleanExtra(PATIENT_ACTIVITY_EDIT_PASS_NAME, false);
        pos = intent.getIntExtra(PatientListActivity.PATIENT_LIST_ITEM_POS_PASS_NAME, -1);
        getSupportActionBar().setTitle(title);


        //init ui components

        patientNameEditText = findViewById(R.id.name_editText_patient);
        patientCodeEditText = findViewById(R.id.r_code_editText_patient);
        patientPhoneEditText = findViewById(R.id.phone_editText_patient);
        patientAddressEditText = findViewById(R.id.address_editText_patient);
        patientDescEditText = findViewById(R.id.description_editText_patient);
        saveButton = findViewById(R.id.save_button_patient);
        cancelButton = findViewById(R.id.cancel_button_patient);

        Intent i = getIntent();
        passPatient = (Patient) i.getParcelableExtra(PatientListActivity.PATIENT_LIST_ITEM_CLICK_PASS_NAME);


        if (passPatient != null && !isEditMode) {
            setPatientInfoForShowing(passPatient);
            makeReadOnlyFields();

        } else if (passPatient != null && isEditMode) {
            setPatientInfoForEditing(passPatient);
            saveButton.setText("Edit");

        }

    }

    private void makeReadOnlyFields() {
        patientNameEditText.setEnabled(false);
        patientCodeEditText.setEnabled(false);
        patientPhoneEditText.setEnabled(false);
        patientAddressEditText.setEnabled(false);
        patientDescEditText.setEnabled(false);
        saveButton.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);

    }

    private void setPatientInfoForShowing(Patient patient) {

        patientNameEditText.setText("Name:   " + patient.getFullName());
        patientCodeEditText.setText("Registration Code:   " + patient.getCode());
        patientPhoneEditText.setText("Phone:   " + patient.getPhone());
        patientAddressEditText.setText("Address:   " + patient.getAddress());
        patientDescEditText.setText("Description:   " + patient.getDescription());


    }

    private void setPatientInfoForEditing(Patient patient) {

        patientNameEditText.setText(patient.getFullName());
        patientCodeEditText.setText(patient.getCode());
        patientPhoneEditText.setText(patient.getPhone());
        patientAddressEditText.setText(patient.getAddress());
        patientDescEditText.setText(patient.getDescription());


    }


    public void handleSaveButtonClick(View view) {

        DatabaseAccess databaseAccess = new DatabaseAccess(this);


        Patient patient = new Patient();

        patient.setFullName(patientNameEditText.getText().toString().trim());
        patient.setCode(patientCodeEditText.getText().toString().trim());
        patient.setPhone(patientPhoneEditText.getText().toString().trim());
        patient.setAddress(patientAddressEditText.getText().toString().trim());
        patient.setDescription(patientDescEditText.getText().toString().trim());


        if (isValidPatientData(patient) && !isEditMode) {

            databaseAccess.insertPatient(patient);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(PATIENT_RESULT_PASS_NAME, patient);
            setResult(PatientActivity.RESULT_OK, returnIntent);
            finish();

        } else if (isValidPatientData(patient) && isEditMode) {

            patient.setId(passPatient.getId());
            boolean isUpdated = databaseAccess.updatePatient(patient);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(PATIENT_RESULT_PASS_NAME, patient);
            setResult(isUpdated ? PATIENT_SUCCESS_UPDATE_RESULT_CODE : PATIENT_FAILD_UPDATE_RESULT_CODE, returnIntent);
            finish();

        }


    }

    public void handleCancelButtonClick(View view) {

        Intent returnIntent = new Intent();
        setResult(PATIENT_CANCEL_RESULT_CODE, returnIntent);
        finish();

    }


    private boolean isValidPatientData(Patient patient) {


        if (patient.getFullName().isEmpty()) {
            myAlert("warning", "Please enter patient full name");
            return false;
        } else if (patient.getCode().isEmpty()) {
            myAlert("warning", "Please enter patient registration code");
            return false;
        } else if (patient.getPhone().isEmpty()) {
            myAlert("warning", "Please enter patient phone number");
            return false;
        } else if (patient.getPhone().length() < 10) {
            myAlert("warning", "Please enter correct patient phone number");
            return false;
        }


        return true;


    }


    private void myAlert(String title, String message) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);

        // add a button
        builder.setPositiveButton("OK", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();


    }


}
