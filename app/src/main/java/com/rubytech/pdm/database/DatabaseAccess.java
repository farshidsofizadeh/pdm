package com.rubytech.pdm.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.rubytech.pdm.model.Patient;
import com.rubytech.pdm.model.PatientSession;

import java.util.ArrayList;

/**
 * Created by Ehsan on 11/20/2018.
 */

public class DatabaseAccess {

    private SQLiteDatabase mDatabase;
    private Database myDb;
    private Context mContext;

    private PatientAccess patientAccess;
    private PatientSessionAccess patientSessionAccess;

    public DatabaseAccess(Context mContext) {
        this.mContext = mContext;

        myDb = new Database(mContext);
    }

    private void openDb() {
        mDatabase = myDb.getWritableDatabase();
    }

    private void closeDb() {
        mDatabase.close();
    }

    public ArrayList<Patient> getAllPatient() {
        try {
            openDb();
            patientAccess = new PatientAccess(mContext, mDatabase);
            return patientAccess.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeDb();
        }

    }

    public Patient getPatientById(int id) {
        Patient patient = new Patient();
        try {
            openDb();
            patientAccess = new PatientAccess(mContext, mDatabase);
            patient = patientAccess.get(id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeDb();
        }

        return patient;

    }

    public Patient getPatientByCode(String code) {
        Patient patient = new Patient();
        try {
            openDb();
            patientAccess = new PatientAccess(mContext, mDatabase);
            patient = patientAccess.get(code);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeDb();
        }

        return patient;
    }

    public void insertPatient(Patient patient) {
        try {
            openDb();
            patientAccess = new PatientAccess(mContext, mDatabase);
            patientAccess.insert(patient);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeDb();
        }
    }

    public boolean updatePatient(Patient patient) {
        try {
            openDb();
            patientAccess = new PatientAccess(mContext, mDatabase);
            return patientAccess.update(patient);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeDb();
        }
    }

    public ArrayList<PatientSession> getAllSessions() {
        try {
            openDb();
            patientSessionAccess = new PatientSessionAccess(mContext, mDatabase);
            return patientSessionAccess.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeDb();
        }
    }

    public ArrayList<PatientSession> getPatientSession(int patientId) {
        try {
            openDb();
            patientSessionAccess = new PatientSessionAccess(mContext, mDatabase);
            return patientSessionAccess.getSessionOF(patientId);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeDb();
        }
    }

    public void insertPatientSession(PatientSession patientSession) {
        try {
            openDb();
            patientSessionAccess = new PatientSessionAccess(mContext, mDatabase);
            patientSessionAccess.insert(patientSession);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeDb();
        }
    }

    public boolean updatePatientSession(PatientSession patientSession) {
        try {
            openDb();
            patientSessionAccess = new PatientSessionAccess(mContext, mDatabase);
            return patientSessionAccess.update(patientSession);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeDb();
        }
    }

    public boolean deletePatientSession(int patientSessionId) {
        try {
            openDb();
            patientSessionAccess = new PatientSessionAccess(mContext, mDatabase);
            return patientSessionAccess.delete(patientSessionId);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeDb();
        }
    }
    
}
