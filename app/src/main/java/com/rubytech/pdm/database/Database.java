package com.rubytech.pdm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Ehsan on 11/20/2018.
 */

public class Database extends SQLiteOpenHelper {


    private static final String CREATE_TABLE_PATIENT = "create table " + DBConstant.PATIENT_TBL_NAME + "(" +
            DBConstant.PATIENT_ID + " INTEGER PRIMARY KEY autoincrement  ," +
            DBConstant.PATIENT_REGISTRATION_CODE + " VARCHAR(20)  ," +
            DBConstant.PATIENT_NAME + " VARCHAR(255)  ," +
            DBConstant.PATIENT_ADDRESS + " VARCHAR(255) ," +
            DBConstant.PATIENT_PHONE + " VARCHAR(13)  ," +
            DBConstant.PATIENT_DESCRIPTION + " TEXT  " +
            " );";


    private static final String CREATE_TABLE_PATIENT_SESSION = "create table " + DBConstant.PATIENT_SESSION_TBL_NAME + "(" +
            DBConstant.PATIENT_SESSION_ID + " INTEGER PRIMARY KEY autoincrement  ," +
            DBConstant.PATIENT_SESSION_PATIENT_ID + " INTEGER ," +
            DBConstant.PATIENT_SESSION_DISEASE + " TEXT  ," +
            DBConstant.PATIENT_SESSION_VISIT_DATE + " LONG ," +
            DBConstant.PATIENT_SESSION_MEDICINE + " TEXT  " +
            " );";


    private static final String DROP_TABLE_PATIENT = "DROP TABLE IF EXISTS " + DBConstant.PATIENT_TBL_NAME;
    private static final String DROP_TABLE_PATIENT_SESSION = "DROP TABLE IF EXISTS " + DBConstant.PATIENT_SESSION_TBL_NAME;


    public Database(Context context) {
        super(context, DBConstant.DATABASE_NAME, null, DBConstant.DB_VERSION);

    }


    @Override
    public void onCreate(SQLiteDatabase sql) {
        sql.execSQL(CREATE_TABLE_PATIENT);
        sql.execSQL(CREATE_TABLE_PATIENT_SESSION);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sql, int i, int i1) {

        sql.execSQL(DROP_TABLE_PATIENT);
        sql.execSQL(DROP_TABLE_PATIENT_SESSION);

        onCreate(sql);


    }
}
