package com.rubytech.pdm.database;

/**
 * Created by Ehsan on 11/20/2018.
 */

public class DBConstant {

    static final String DATABASE_NAME = "PDM.db";
    static final int DB_VERSION = 1;


    static final String PATIENT_TBL_NAME = "patient";
    static final String PATIENT_ID = "_id";
    static final String PATIENT_REGISTRATION_CODE = "registration_code";
    static final String PATIENT_NAME = "name";
    static final String PATIENT_ADDRESS = "address";
    static final String PATIENT_PHONE = "phone_number";
    static final String PATIENT_DESCRIPTION = "description";


    static final String PATIENT_SESSION_TBL_NAME = "patient_session";
    static final String PATIENT_SESSION_ID = "id";
    static final String PATIENT_SESSION_PATIENT_ID = "patient_id";
    static final String PATIENT_SESSION_DISEASE = "disease";
    static final String PATIENT_SESSION_VISIT_DATE = "visit_date";
    static final String PATIENT_SESSION_MEDICINE = "medicine";


}
