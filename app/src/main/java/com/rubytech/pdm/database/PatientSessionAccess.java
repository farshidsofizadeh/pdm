package com.rubytech.pdm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rubytech.pdm.model.Patient;
import com.rubytech.pdm.model.PatientSession;

import java.util.ArrayList;

/**
 * Created by Ehsan on 11/20/2018.
 */

class PatientSessionAccess {

    private Context context;
    private SQLiteDatabase mDatabase;
    private String tableName = DBConstant.PATIENT_SESSION_TBL_NAME;

    private static final String[] PATIENT_COLUMNS = {
            /*0*/DBConstant.PATIENT_ID,
            /*1*/DBConstant.PATIENT_REGISTRATION_CODE,
            /*2*/DBConstant.PATIENT_NAME,
            /*3*/DBConstant.PATIENT_ADDRESS,
            /*4*/DBConstant.PATIENT_PHONE,
            /*5*/DBConstant.PATIENT_DESCRIPTION

    };
    private static final String[] COLUMNS = {
            /*0*/DBConstant.PATIENT_SESSION_ID,
            /*1*/DBConstant.PATIENT_SESSION_PATIENT_ID,
            /*2*/DBConstant.PATIENT_SESSION_DISEASE,
            /*3*/DBConstant.PATIENT_SESSION_VISIT_DATE,
            /*4*/DBConstant.PATIENT_SESSION_MEDICINE
    };

    PatientSessionAccess(Context context, SQLiteDatabase mDatabase) {
        this.context = context;
        this.mDatabase = mDatabase;
    }


    /**
     * this will query all patient sessions and its patient from database
     *
     * @return list of patientSession
     */
    public ArrayList<PatientSession> getAll() {
        ArrayList<PatientSession> patientSessions = new ArrayList<>();

        String query = "SELECT " + DBConstant.PATIENT_TBL_NAME + "." + DBConstant.PATIENT_NAME +
                "," + DBConstant.PATIENT_TBL_NAME + "." + DBConstant.PATIENT_REGISTRATION_CODE +
                ", " + DBConstant.PATIENT_SESSION_TBL_NAME + "." + DBConstant.PATIENT_SESSION_DISEASE +
                "," + DBConstant.PATIENT_SESSION_TBL_NAME + "." + DBConstant.PATIENT_SESSION_VISIT_DATE +
                "," + DBConstant.PATIENT_SESSION_TBL_NAME + "." + DBConstant.PATIENT_SESSION_MEDICINE + "" +
                "," + DBConstant.PATIENT_SESSION_TBL_NAME + "." + DBConstant.PATIENT_SESSION_ID +
                " FROM " + DBConstant.PATIENT_SESSION_TBL_NAME + " INNER JOIN " + DBConstant.PATIENT_TBL_NAME +
                " ON " + DBConstant.PATIENT_SESSION_TBL_NAME + "." + DBConstant.PATIENT_SESSION_PATIENT_ID +
                "=" + DBConstant.PATIENT_TBL_NAME + "." + DBConstant.PATIENT_ID + " ORDER BY " +
                DBConstant.PATIENT_SESSION_TBL_NAME + "." + DBConstant.PATIENT_SESSION_VISIT_DATE + " DESC ;";

        Cursor cursor = mDatabase.rawQuery(query, null);

        while (cursor.moveToNext()) {
            patientSessions.add(cursorToObject(cursor, true));
        }

        return patientSessions;
    }

    /**
     * this will query all session of patient by given patientId
     *
     * @param patientId id of patient
     * @return list of sessions;
     */
    public ArrayList<PatientSession> getSessionOF(int patientId) {
        ArrayList<PatientSession> patientSessions = new ArrayList<>();

        String query = "SELECT ps.* , p.* FROM " + tableName + " ps " +
                "INNER JOIN " + DBConstant.PATIENT_TBL_NAME + " p ON ps." + COLUMNS[0] + " = '" + patientId + "' ; ";

        Cursor cursor = mDatabase.rawQuery(query, null);

        while (cursor.moveToNext()) {
            patientSessions.add(cursorToObject(cursor, false));
        }

        return patientSessions;

    }

    /**
     * this will insert a patientSession into table and set id into object
     *
     * @param patientSession object to be store
     */
    public void insert(PatientSession patientSession) {
        long id = 0;

        // sending the object to convert it into a content value object
        ContentValues cv = patientSessionToCV(patientSession);


        id = mDatabase.insert(tableName, null, cv);

        patientSession.setId((int) id);
    }

    /**
     * this will update the given patientSession into database
     *
     * @param patientSession the given object to be update
     * @return true if any row affected and query execute successfully
     */
    public boolean update(PatientSession patientSession) {

        ContentValues cv = patientSessionToCV(patientSession);

        int affectedRows = mDatabase.update(tableName, cv, COLUMNS[0] + " = '" + patientSession.getId() + "'", null);


        return affectedRows > 0;
    }


    /**
     * this will delete the object of given id
     *
     * @param patientSessionId object id
     * @return true if some row are deleted
     */
    public boolean delete(int patientSessionId) {

        int affectedRows = mDatabase.delete(tableName, COLUMNS[0] + " = '" + patientSessionId + "'", null);

        return affectedRows > 0;
    }


    /**
     * this will convert patient session object into a content value object without patient session id
     *
     * @param patientSession given object
     * @return content value filled with given patient data
     */
    private ContentValues patientSessionToCV(PatientSession patientSession) {

        ContentValues cv = new ContentValues();

        // DO NOT PUT ID INTO CONTENT VALUE HERE.

        //i check if there is a data in the fields for preventing black data overriding during update
        if (patientSession.getPatient().getId() != 0)
            cv.put(COLUMNS[1], patientSession.getPatient().getId());
        if (patientSession.getDisease().length() > 0)
            cv.put(COLUMNS[2], patientSession.getDisease());
        if (patientSession.getVisitDate() != 0)
            cv.put(COLUMNS[3], patientSession.getVisitDate());
        if (patientSession.getMedicine().length() > 0)
            cv.put(COLUMNS[4], patientSession.getMedicine());


        return cv;
    }


    /**
     * this will convert cursor data into PatientSession object with its patient
     *
     * @param cursor
     * @param b
     * @return
     */
    private PatientSession cursorToObject(Cursor cursor, boolean b) {
        PatientSession patientSession = new PatientSession();

        if (b) {
            Patient patient = new Patient();
            patient.setCode(cursor.getString(cursor.getColumnIndex(DBConstant.PATIENT_REGISTRATION_CODE)));
            patient.setFullName(cursor.getString(cursor.getColumnIndex(DBConstant.PATIENT_NAME)));
            patientSession.setPatient(patient);

        }
        patientSession.setId(cursor.getInt(cursor.getColumnIndex(COLUMNS[0])));
        patientSession.setDisease(cursor.getString(cursor.getColumnIndex(COLUMNS[2])));
        patientSession.setVisitDate(cursor.getLong(cursor.getColumnIndex(COLUMNS[3])));
        patientSession.setMedicine(cursor.getString(cursor.getColumnIndex(COLUMNS[4])));


        return patientSession;
    }


}

