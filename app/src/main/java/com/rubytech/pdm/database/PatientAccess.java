package com.rubytech.pdm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rubytech.pdm.model.Patient;

import java.util.ArrayList;

/**
 * Created by Ehsan on 11/20/2018.
 * .
 */

class PatientAccess {

    private Context context;
    private SQLiteDatabase mDatabase;
    private String tableName = DBConstant.PATIENT_TBL_NAME;


    private static final String[] COLUMNS = {
            /*0*/DBConstant.PATIENT_ID,
            /*1*/DBConstant.PATIENT_REGISTRATION_CODE,
            /*2*/DBConstant.PATIENT_NAME,
            /*3*/DBConstant.PATIENT_ADDRESS,
            /*4*/DBConstant.PATIENT_PHONE,
            /*5*/DBConstant.PATIENT_DESCRIPTION

    };

    PatientAccess(Context context, SQLiteDatabase mDatabase) {
        this.context = context;
        this.mDatabase = mDatabase;
    }


    /**
     * this will query all patients from table
     *
     * @return array list of all patient
     */
    ArrayList<Patient> getAll() {
        ArrayList<Patient> patients = new ArrayList<>();


        Cursor cursor = mDatabase.query(tableName, COLUMNS, null, null, null, null, null);

        while (cursor.moveToNext()) {
            patients.add(cursorToPatient(cursor));
        }

        return patients;
    }

    /**
     * this will query the patient by its id
     *
     * @param id patient id
     * @return patient object
     */
    Patient get(int id) {

        String query = "select * from " + tableName + " where " + COLUMNS[0] + " = '" + id + "' ;";
        Cursor cursor = mDatabase.rawQuery(query, null);

        while (cursor.moveToNext()) {
            return cursorToPatient(cursor);
        }

        return null;
    }


    /**
     * this will query the patient by its Registered code
     *
     * @param registerCode patient code
     * @return patient object
     */
    Patient get(String registerCode) {

        String query = "select * from " + tableName + " where " + COLUMNS[1] + " = '" + registerCode + "' ;";
        Cursor cursor = mDatabase.rawQuery(query, null);

        while (cursor.moveToNext()) {
            return cursorToPatient(cursor);
        }

        return null;
    }


    /**
     * this will get a patient object and store that into patient table and store generated id into object
     *
     * @param patient given object to be store
     */
    void insert(Patient patient) {
        long id = 0;

        // sending the object to convert it into a content value object
        ContentValues cv = patientToCV(patient);

        id = mDatabase.insert(tableName, null, cv);

        patient.setId((int) id);

    }


    /**
     * this will update the given patient into database
     *
     * @param patient the given object to be update
     * @return true if any row affected and query execute successfully
     */
    boolean update(Patient patient) {

        // sending the object to convert it into a content value object
        ContentValues cv = patientToCV(patient);

        //update the patient by its id
        int numberOfRows = mDatabase.update(tableName, cv, COLUMNS[0] + " = " + patient.getId(), null);

        return numberOfRows > 0;
    }


    /**
     * this will convert patient object into a content value object without patient id
     *
     * @param patient given object
     * @return content value filled with given patient data
     */
    private ContentValues patientToCV(Patient patient) {

        ContentValues cv = new ContentValues();

        // DO NOT PUT ID INTO CONTENT VALUE HERE.

        //i check if there is a data in the fields for preventing black data overriding during update
        if (patient.getCode().length() > 0)
            cv.put(COLUMNS[1], patient.getCode());
        if (patient.getFullName().length() > 0)
            cv.put(COLUMNS[2], patient.getFullName());
        if (patient.getAddress().length() > 0)
            cv.put(COLUMNS[3], patient.getAddress());
        if (patient.getPhone().length() > 0)
            cv.put(COLUMNS[4], patient.getPhone());
        if (patient.getDescription().length() > 0)
            cv.put(COLUMNS[5], patient.getDescription());


        return cv;
    }

    /**
     * this will convert cursor data into a patient object
     *
     * @param cursor given cursor data
     * @return created patient object
     */
    private Patient cursorToPatient(Cursor cursor) {

        Patient patient = new Patient();

        patient.setId(cursor.getInt(cursor.getColumnIndex(COLUMNS[0])));
        patient.setCode(cursor.getString(cursor.getColumnIndex(COLUMNS[1])));
        patient.setFullName(cursor.getString(cursor.getColumnIndex(COLUMNS[2])));
        patient.setAddress(cursor.getString(cursor.getColumnIndex(COLUMNS[3])));
        patient.setPhone(cursor.getString(cursor.getColumnIndex(COLUMNS[4])));
        patient.setDescription(cursor.getString(cursor.getColumnIndex(COLUMNS[5])));


        return patient;
    }


}
