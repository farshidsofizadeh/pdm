package com.rubytech.pdm.model;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rubytech.pdm.MainActivity;
import com.rubytech.pdm.PatientSessionActivity;
import com.rubytech.pdm.R;

import java.util.ArrayList;

public class PatientSessionAdapter extends ArrayAdapter<PatientSession> {

    private Context mContext;
    private int resourceLayout;
    public static int PATIENT_SESSION_LIST_ACTIVITY_EDIT_REQUEST_CODE = 6;
    private ArrayList<PatientSession> mPatientSessions;

    public PatientSessionAdapter(@NonNull Context context, int resource, ArrayList<PatientSession> patientSessions) {
        super(context, resource);
        this.mContext = context;
        this.resourceLayout = resource;
        this.mPatientSessions = patientSessions;

    }


    @Override
    public int getCount() {
        return mPatientSessions.size();
    }


    @Override
    public PatientSession getItem(int location) {
        return mPatientSessions.get(location);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        final int pos = position;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(resourceLayout, null);
        }

        final PatientSession patientSession = mPatientSessions.get(position);
        TextView nameTextView = null, codeTextView = null, dateTextView = null;
        ImageButton editButton = null, deleteButton = null;

        if (patientSession != null) {

            nameTextView = (TextView) view.findViewById(R.id.patient_session__name_textView_custom_item);
            codeTextView = (TextView) view.findViewById(R.id.patient_session_r_code_textView_custom_item);
            dateTextView = (TextView) view.findViewById(R.id.date_textView_custom_item);
            editButton = view.findViewById(R.id.edit_patient_session_button);
            deleteButton = view.findViewById(R.id.delete_patient_session_button);
        }

        if (nameTextView != null) {
            nameTextView.setText(patientSession.getPatient().getFullName());
        }
        if (codeTextView != null) {
            codeTextView.setText(patientSession.getPatient().getCode());
        }

        if (dateTextView != null) {

            dateTextView.setText(patientSession.getDateAsStr());
        }

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PatientSessionActivity.class);
                intent.putExtra(MainActivity.PATIENT_SESSION_ACTIVITY_TITLE_PASS_NAME, "Edit Patient Session");
                intent.putExtra(MainActivity.PATIENT_SESSION_LIST_ITEM_CLICK_PASS_NAME, patientSession);
                intent.putExtra(MainActivity.PATIENT_SESSION_LIST_ITEM_POS_PASS_NAME, pos);
                intent.putExtra(MainActivity.PATIENT_SESSION_ACTIVITY_EDIT_PASS_NAME, true);

                ((Activity) mContext).startActivityForResult(intent, PATIENT_SESSION_LIST_ACTIVITY_EDIT_REQUEST_CODE);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PatientSessionActivity.class);
                intent.putExtra(MainActivity.PATIENT_SESSION_ACTIVITY_TITLE_PASS_NAME, "Delete Patient Session");
                intent.putExtra(MainActivity.PATIENT_SESSION_LIST_ITEM_CLICK_PASS_NAME, patientSession);
                intent.putExtra(MainActivity.PATIENT_SESSION_LIST_ITEM_POS_PASS_NAME, pos);
                intent.putExtra(MainActivity.PATIENT_SESSION_ACTIVITY_Delete_PASS_NAME, true);

                ((Activity) mContext).startActivityForResult(intent, PATIENT_SESSION_LIST_ACTIVITY_EDIT_REQUEST_CODE);
            }
        });


        return view;

    }

    public void update(ArrayList<PatientSession> patientSessions) {

        mPatientSessions = new ArrayList<>();
        mPatientSessions.addAll(patientSessions);
        notifyDataSetChanged();
    }
}
