package com.rubytech.pdm.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rubytech.pdm.PatientActivity;
import com.rubytech.pdm.PatientListActivity;
import com.rubytech.pdm.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Farshid on 11/24/2018.
 */

public class PatientListAdapter extends ArrayAdapter<Patient> {


    private Context mContext;
    private int resourceLayout;
    public static int PATIENT_LIST_ACTIVITY_EDIT_REQUEST_CODE = 3;
    private ArrayList<Patient> mPatients;


    public PatientListAdapter(@NonNull Context context, int resource, ArrayList<Patient> patients) {
        super(context, resource);

        this.mContext = context;
        this.resourceLayout = resource;
        this.mPatients = patients;
    }

    public void update(ArrayList<Patient> patients) {

        mPatients = new ArrayList<>();
        mPatients.addAll(patients);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mPatients.size();
    }


    @Override
    public Patient getItem(int location) {
        return mPatients.get(location);
    }

    @NonNull
    @Override
    public View getView( int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        final int pos=position;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(resourceLayout, null);
        }

        final Patient patient = mPatients.get(position);
        TextView nameTextView = null, codeTextView = null;
        ImageButton editButton = null;

        if (patient != null) {

            nameTextView = (TextView) view.findViewById(R.id.name_textView_patient_item);
            codeTextView = (TextView) view.findViewById(R.id.r_code_Patient_textView_item);
            editButton = view.findViewById(R.id.edit_patient_button);
        }

        if (nameTextView != null) {
            nameTextView.setText(patient.getFullName());
        }
        if (codeTextView != null) {
            codeTextView.setText(patient.getCode());
        }

        if (editButton != null) {

            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, PatientActivity.class);
                    intent.putExtra(PatientListActivity.PATIENT_ACTIVITY_TITLE_PASS_NAME, "Edit Patient");
                    intent.putExtra(PatientListActivity.PATIENT_LIST_ITEM_CLICK_PASS_NAME, patient);
                    intent.putExtra(PatientListActivity.PATIENT_LIST_ITEM_POS_PASS_NAME, pos);
                    intent.putExtra(PatientActivity.PATIENT_ACTIVITY_EDIT_PASS_NAME, true);

                    ((Activity) mContext).startActivityForResult(intent, PATIENT_LIST_ACTIVITY_EDIT_REQUEST_CODE);
                }
            });

        }

        return view;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
