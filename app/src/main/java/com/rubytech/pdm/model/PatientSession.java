package com.rubytech.pdm.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Ehsan on 11/20/2018.
 */

public class PatientSession implements Parcelable {

    private int id;
    private Patient patient;
    private String disease;
    private long visitDate;
    private String medicine;

    public PatientSession() {
        this.id = 0;
        this.patient = new Patient();
        this.disease = "";
        this.visitDate = 0;
        this.medicine = "";
    }

    public PatientSession(int id, Patient patient, String disease, long visitDate, String medicine) {
        this.id = id;
        this.patient = patient;
        this.disease = disease;
        this.visitDate = visitDate;
        this.medicine = medicine;
    }

    private PatientSession(Parcel in) {
        id = in.readInt();
        patient = in.readParcelable(Patient.class.getClassLoader());
        disease = in.readString();
        visitDate = in.readLong();
        medicine = in.readString();
    }

    public static final Creator<PatientSession> CREATOR = new Creator<PatientSession>() {
        @Override
        public PatientSession createFromParcel(Parcel in) {
            return new PatientSession(in);
        }

        @Override
        public PatientSession[] newArray(int size) {
            return new PatientSession[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public long getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(long visitDate) {
        this.visitDate = visitDate;
    }

    public String getMedicine() {
        return medicine;
    }

    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    @Override
    public String toString() {
        return "PatientSession{" +
                "id=" + id +
                ", patient=" + patient +
                ", disease='" + disease + '\'' +
                ", visitDate=" + visitDate +
                ", medicine='" + medicine + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeParcelable(patient, i);
        parcel.writeString(disease);
        parcel.writeLong(visitDate);
        parcel.writeString(medicine);
    }

    public String getDateAsStr() {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getVisitDate());
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        return sdf.format(calendar.getTime());
    }

    @Override
    public boolean equals(Object patientSession) {
        if (!(patientSession instanceof PatientSession)) {
            return false;
        }
        PatientSession that = (PatientSession) patientSession;
        return this.getId() == (that.getId());
    }
}
