package com.rubytech.pdm.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Ehsan on 11/14/2018.
 */

public class Patient implements Parcelable {

    private int id;
    private String code;
    private String fullName;
    private String address;
    private String phone;
    private String description;

    public Patient(int id, String code, String fullName, String address, String phone, String description) {
        this.id = id;
        this.code = code;
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.description = description;
    }

    public Patient() {
        this.id = 0;
        this.code = "";
        this.fullName = "";
        this.address = "";
        this.phone = "";
        this.description = "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(code);
        parcel.writeString(fullName);
        parcel.writeString(address);
        parcel.writeString(phone);
        parcel.writeString(description);
    }


    private Patient(Parcel in) {
        id = in.readInt();
        code = in.readString();
        fullName = in.readString();
        address = in.readString();
        phone = in.readString();
        description = in.readString();
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel in) {
            return new Patient(in);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return getFullName() + " ( " + getCode() + " )";


    }


}
