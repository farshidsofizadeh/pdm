package com.rubytech.pdm.controller;

import android.content.Context;
import android.support.v7.app.AlertDialog;

/**
 * Created by Farshid on 11/29/2018.
 */

public class MyAlert {


    public static void myAlert(String title, String message, Context context) {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        // add a button
        builder.setPositiveButton("OK", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();


    }
}
