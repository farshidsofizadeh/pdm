package com.rubytech.pdm;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.rubytech.pdm.controller.MyAlert;
import com.rubytech.pdm.database.DatabaseAccess;
import com.rubytech.pdm.model.Patient;
import com.rubytech.pdm.model.PatientSession;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PatientSessionActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView dateTextView;
    private EditText medicine, disease;
    private Spinner spinner;
    private Button cancelButton, saveButton;
    private ImageButton newPatientImageButton;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private ArrayList<Patient> mPatient;
    public static int START_PATIENT_ACTIVITY_REQUEST_CODE_FROM_PS = 7;
    public static final int PATIENT_SESSION_CANCEL_RESULT_CODE = 8;
    public static int PATIENT_SESSION_SUCCESS_UPDATE_RESULT_CODE = 10;
    public static int PATIENT_SESSION_SUCCESS_DELETE_RESULT_CODE = 11;
    private ArrayAdapter<Patient> patientArrayAdapter;
    private int year;
    private int month;
    private int day;
    public static final String PATIENT_SESSTION_RESULT_PASS_NAME = "patientSessionResult";
    private PatientSession passPatientSession = null;

    private boolean isEditMode = false;
    private boolean isDeleteMode = false;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_session);


        dateTextView = (TextView) findViewById(R.id.date_TextView_patient_session);
        medicine = (EditText) findViewById(R.id.medicine_editText);
        disease = (EditText) findViewById(R.id.disease_editText);
        spinner = (Spinner) findViewById(R.id.patient_spinner);
        cancelButton = findViewById(R.id.cancel_button_patient_session);
        saveButton = findViewById(R.id.save_button_patient_session);
        newPatientImageButton = findViewById(R.id.add_new_patient_button);

        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);

        month = month + 1;
        String date = month + "/" + day + "/" + year;
        dateTextView.setText(date);

        dateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                DatePickerDialog dialog = new DatePickerDialog(
                        PatientSessionActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });


        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                String date = month + "/" + day + "/" + year;
                dateTextView.setText(date);
            }
        };


        // Attaching the layout to the toolbar object
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);


        Intent intent = getIntent();
        String title = intent.getStringExtra(MainActivity.PATIENT_SESSION_ACTIVITY_TITLE_PASS_NAME);
        isEditMode = intent.getBooleanExtra(MainActivity.PATIENT_SESSION_ACTIVITY_EDIT_PASS_NAME, false);
        isDeleteMode = intent.getBooleanExtra(MainActivity.PATIENT_SESSION_ACTIVITY_Delete_PASS_NAME, false);
        pos = intent.getIntExtra(MainActivity.PATIENT_SESSION_LIST_ITEM_POS_PASS_NAME, -1);
        getSupportActionBar().setTitle(title);


        DatabaseAccess databaseAccess = new DatabaseAccess(this);
        mPatient = databaseAccess.getAllPatient();


        patientArrayAdapter = new ArrayAdapter<Patient>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, mPatient);
        patientArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(patientArrayAdapter);

        Intent i = getIntent();
        passPatientSession = (PatientSession) i.getParcelableExtra(MainActivity.PATIENT_SESSION_LIST_ITEM_CLICK_PASS_NAME);


        if (passPatientSession != null && !isEditMode && !isDeleteMode) {

            setPatientSessionForDisplay(passPatientSession);
            setControllButtonHide();

        } else if (passPatientSession != null && isEditMode && !isDeleteMode) {

            setPatientSessionForEdit(passPatientSession);

        } else if (passPatientSession != null && isDeleteMode) {
            setPatientSessionForDisplay(passPatientSession);
            saveButton.setText("Delete");
        }


    }

    private void setPatientSessionForEdit(PatientSession passPatientSession) {

        spinner.setSelection(getIndex(spinner, passPatientSession.getPatient().toString()));
        disease.setText(passPatientSession.getDisease());
        medicine.setText(passPatientSession.getMedicine());
        dateTextView.setText(passPatientSession.getDateAsStr());
        saveButton.setText("Edit");


    }

    private void setPatientSessionForDisplay(PatientSession passPatientSession) {

        spinner.setSelection(getIndex(spinner, passPatientSession.getPatient().toString()));
        spinner.setEnabled(false);
        newPatientImageButton.setVisibility(View.INVISIBLE);

        disease.setText("Diseases:  " + passPatientSession.getDisease());
        medicine.setText("Medicines:  " + passPatientSession.getMedicine());
        dateTextView.setText(passPatientSession.getDateAsStr());
        disease.setEnabled(false);
        medicine.setEnabled(false);
        dateTextView.setEnabled(false);

    }

    private void setControllButtonHide() {

        saveButton.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);

    }


    private int getIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }


    public void handlePSSaveButtonClick(View view) {


        PatientSession patientSession = new PatientSession();
        Patient patient = (Patient) spinner.getSelectedItem();

        patientSession.setPatient(patient);
        patientSession.setMedicine(medicine.getText().toString().trim());
        patientSession.setDisease(disease.getText().toString().trim());


        long milliseconds = 0;

        SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date d = f.parse(dateTextView.getText().toString());
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        patientSession.setVisitDate(milliseconds);


        if (isValid(patientSession) && !isEditMode && !isDeleteMode) {

            DatabaseAccess databaseAccess = new DatabaseAccess(this);
            databaseAccess.insertPatientSession(patientSession);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(PATIENT_SESSTION_RESULT_PASS_NAME, patientSession);
            setResult(PatientActivity.RESULT_OK, returnIntent);
            finish();

        } else if (isValid(patientSession) && isEditMode) {

            patientSession.setId(passPatientSession.getId());

            DatabaseAccess databaseAccess = new DatabaseAccess(this);
            databaseAccess.updatePatientSession(patientSession);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(PATIENT_SESSTION_RESULT_PASS_NAME, patientSession);
            setResult(PATIENT_SESSION_SUCCESS_UPDATE_RESULT_CODE, returnIntent);
            finish();


        } else if (isValid(patientSession) && isDeleteMode) {

            patientSession.setId(passPatientSession.getId());
            DatabaseAccess databaseAccess = new DatabaseAccess(this);
            databaseAccess.deletePatientSession(patientSession.getId());
            Intent returnIntent = new Intent();
            returnIntent.putExtra(PATIENT_SESSTION_RESULT_PASS_NAME, patientSession);
            setResult(PATIENT_SESSION_SUCCESS_DELETE_RESULT_CODE, returnIntent);
            finish();

        }


    }

    private boolean isValid(PatientSession patientSession) {


        if (patientSession.getPatient() == null) {
            MyAlert.myAlert("Information", "please select a patient!", this);
            return false;
        } else if (patientSession.getDisease().isEmpty()) {
            MyAlert.myAlert("Information", "please enter diseases!", this);
            return false;
        } else if (patientSession.getMedicine().isEmpty()) {
            MyAlert.myAlert("Information", "please enter medicines!", this);
            return false;
        } else if (patientSession.getVisitDate() == 0) {
            MyAlert.myAlert("Information", "please select visit date!", this);
            return false;
        }

        return true;
    }

    public void handlePSCancleButtonClick(View view) {

        Intent returnIntent = new Intent();
        setResult(PATIENT_SESSION_CANCEL_RESULT_CODE, returnIntent);
        finish();

    }


    public void handleAddingNewPatientClick(View view) {

        Intent intent = new Intent(this, PatientActivity.class);
        intent.putExtra(PatientListActivity.PATIENT_ACTIVITY_TITLE_PASS_NAME, "Add Patient");
        startActivityForResult(intent, START_PATIENT_ACTIVITY_REQUEST_CODE_FROM_PS);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == START_PATIENT_ACTIVITY_REQUEST_CODE_FROM_PS &&
                resultCode == PatientSessionActivity.RESULT_OK && data != null) {


            Patient patient = (Patient) data.getParcelableExtra(PatientActivity.PATIENT_RESULT_PASS_NAME);
            mPatient.add(patient);
            this.patientArrayAdapter.notifyDataSetChanged();
            MyAlert.myAlert("information", "Patient with registration code ( " + patient.getCode() + " ) has been added successfully", this);


        }
    }
}
