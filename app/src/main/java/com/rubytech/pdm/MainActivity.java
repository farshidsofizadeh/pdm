package com.rubytech.pdm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.rubytech.pdm.controller.MyAlert;
import com.rubytech.pdm.database.Database;
import com.rubytech.pdm.database.DatabaseAccess;
import com.rubytech.pdm.model.PatientSession;
import com.rubytech.pdm.model.PatientSessionAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public static final int START_PATIENT_SESSION_ACTIVITY_REQUEST_CODE = 2;
    private ArrayList<PatientSession> patientSessions;
    private ListView listView;
    private PatientSessionAdapter patientSessionAdapter;
    public static final String PATIENT_SESSION_LIST_ITEM_CLICK_PASS_NAME = "patientSessionItem";
    public static final String PATIENT_SESSION_ACTIVITY_TITLE_PASS_NAME = "patientSessionTitle";
    public static String PATIENT_SESSION_LIST_ITEM_POS_PASS_NAME = "patientSessionPos";
    public static String PATIENT_SESSION_ACTIVITY_EDIT_PASS_NAME = "edit";
    public static String PATIENT_SESSION_ACTIVITY_Delete_PASS_NAME = "delete";
    private MenuItem item;
    private SearchView actionSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Attaching the layout to the toolbar object
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        // Setting toolbar as the ActionBar with setSupportActionBar() call
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.patient_session_listView);


        //creating db
        Database database = new Database(this);
        database.getWritableDatabase();

        DatabaseAccess databaseAccess = new DatabaseAccess(this);
        patientSessions = databaseAccess.getAllSessions();


        patientSessionAdapter = new PatientSessionAdapter(this, R.layout.custom_patient_session_item, patientSessions);
        listView.setAdapter(patientSessionAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {

                PatientSession patientSession = (PatientSession) patientSessionAdapter.getItem(position);
                Intent intent = new Intent(MainActivity.this, PatientSessionActivity.class);
                intent.putExtra(PATIENT_SESSION_LIST_ITEM_CLICK_PASS_NAME, patientSession);
                intent.putExtra(PATIENT_SESSION_ACTIVITY_TITLE_PASS_NAME, "Patient Session Info");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        item = menu.findItem(R.id.action_search);
        actionSearch = (SearchView) item.getActionView();

        actionSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                ArrayList<PatientSession> result = new ArrayList<>();

                for (PatientSession p : patientSessions) {

                    if (p.getPatient().getFullName().toLowerCase().contains(newText.toLowerCase())) {
                        result.add(p);
                    }
                }

                ((PatientSessionAdapter) listView.getAdapter()).update(result);


                return false;
            }
        });


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_patient_list:

                Intent intent = new Intent(this, PatientListActivity.class);
                startActivity(intent);

                break;
            case R.id.action_patient_session:
                Intent i = new Intent(this, PatientSessionActivity.class);
                i.putExtra(PATIENT_SESSION_ACTIVITY_TITLE_PASS_NAME, "Add Patient Session");
                startActivityForResult(i, START_PATIENT_SESSION_ACTIVITY_REQUEST_CODE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == START_PATIENT_SESSION_ACTIVITY_REQUEST_CODE &&
                resultCode == PatientSessionActivity.RESULT_OK && data != null) {

            PatientSession patientSession = (PatientSession) data.getParcelableExtra(PatientSessionActivity.PATIENT_SESSTION_RESULT_PASS_NAME);
            patientSessions.add(0, patientSession);
            patientSessionAdapter.notifyDataSetChanged();
            actionSearch.setQuery("", false);
            actionSearch.clearFocus();
            MyAlert.myAlert("information", "Patient Session with registration code ( " + patientSession.getPatient().getCode() + " ) has been added successfully", this);

        } else if (requestCode == START_PATIENT_SESSION_ACTIVITY_REQUEST_CODE &&
                resultCode == PatientSessionActivity.PATIENT_SESSION_CANCEL_RESULT_CODE && data != null) {
            MyAlert.myAlert("information", "Addition of new patient session has been canceled successfully", this);


        } else if (requestCode == PatientSessionAdapter.PATIENT_SESSION_LIST_ACTIVITY_EDIT_REQUEST_CODE
                && resultCode == PatientSessionActivity.PATIENT_SESSION_SUCCESS_UPDATE_RESULT_CODE && data != null) {

            PatientSession patientSession = (PatientSession) data.getParcelableExtra(PatientSessionActivity.PATIENT_SESSTION_RESULT_PASS_NAME);

            DatabaseAccess databaseAccess = new DatabaseAccess(this);
            patientSessions = databaseAccess.getAllSessions();

            patientSessionAdapter = new PatientSessionAdapter(this, R.layout.custom_patient_session_item, patientSessions);
            listView.setAdapter(patientSessionAdapter);
            actionSearch.setQuery("", false);
            actionSearch.clearFocus();
            MyAlert.myAlert("information", "Patient Session with registration code ( " + patientSession.getPatient().getCode() + " ) has been updated successfully", this);

        } else if (requestCode == PatientSessionAdapter.PATIENT_SESSION_LIST_ACTIVITY_EDIT_REQUEST_CODE
                && resultCode == PatientSessionActivity.PATIENT_SESSION_SUCCESS_DELETE_RESULT_CODE && data != null) {

            PatientSession patientSession = (PatientSession) data.getParcelableExtra(PatientSessionActivity.PATIENT_SESSTION_RESULT_PASS_NAME);
            patientSessions.remove(patientSession);
            patientSessionAdapter.notifyDataSetChanged();
            actionSearch.setQuery("", false);
            actionSearch.clearFocus();
            MyAlert.myAlert("information", "Patient Session with registration code ( " + patientSession.getPatient().getCode() + " ) has been deleted successfully", this);


        }


    }
}
